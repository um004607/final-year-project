# -*- coding: utf-8 -*-
"""
Created on Sun Feb  5 14:31:45 2023

@author: seang
"""
#The packages that are needed to be imported
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
import math

def EuclideanDistance(pca1, pca2, count, centroids):
    EuclideanDistance0 = []
    if pca1[-1] == 0:    
        for i in range (0,count-1):
            distance = (((centroids[0,0]-pca1[i])**2)+((centroids[0,1]-pca2[i])**2))
            EuclideanDistance0.append(distance)
        return EuclideanDistance0
    elif pca1[-1] == 1:
        for i in range (0,count-1):
            distance = (((centroids[1,0]-pca1[i])**2)+((centroids[1,1]-pca2[i])**2))
            EuclideanDistance0.append(distance)
        return EuclideanDistance0
    elif pca1[-1] == 2:
        for i in range (0,count-1):
            distance = (((centroids[2,0]-pca1[i])**2)+((centroids[2,1]-pca2[i])**2))
            EuclideanDistance0.append(distance)
        return EuclideanDistance0
    elif pca1[-1] == 3:
        for i in range (0,count-1):
            distance = (((centroids[3,0]-pca1[i])**2)+((centroids[3,1]-pca2[i])**2))
            EuclideanDistance0.append(distance)
        return EuclideanDistance0
    elif pca1[-1] == 4:
        for i in range (0,count-1):
            distance = (((centroids[4,0]-pca1[i])**2)+((centroids[4,1]-pca2[i])**2))
            EuclideanDistance0.append(distance)
        return EuclideanDistance0
    else:
        return 0
    

    
    
#Importing the csv file and saving it as a pandas data frame
traitDF = pd.read_csv('./data/Feature_result.csv',dtype=int,delimiter=',')
#Getting a random 1% of the data set
traitData = traitDF.sample(frac=0.01)

#Performs PCA feature selection on the data reducing it down to 2 columns
pca = PCA(n_components=2).fit_transform(traitData)

dbscanPCA = pca
#Performs the k-means clustering of the data set using the values from
#the elbow method graph and fits the model to the dataset
#kmeans = KMeans(n_clusters=3).fit(pca)
#kmeans = KMeans(n_clusters=4).fit(pca)
kmeans = KMeans(n_clusters=5).fit(pca)
#Creates a variable to store all of the cluster labels for each data point
KmeansLabels = kmeans.labels_
#Stores the co-ordinates of the centroids as a Data Frame
centroids = kmeans.cluster_centers_
centroidsDF = pd.DataFrame(kmeans.cluster_centers_,columns=["X","Y"])

#Stores the predicted class labels of the PCA dataset
pred=kmeans.predict(pca)
#Sets the labels for the x and y axis
plt.xlabel("PCA1")
plt.ylabel("PCA2")
plt.title("K-means")
#Plots the graph with the prediction model fitted to the graph
kmeansplt=plt.scatter(pca[:,0],pca[:,1],cmap="jet",c=pred,linewidths=1,
                      edgecolors='black')


#Stores the predicted class labels for the centroids which will be their actual
#labels
centroidPred=kmeans.predict(centroids)
#Adds the class labels to the centroid data frame 
centroidsDF["Cluster"]=centroidPred
#Plots the centroids onto the same graph as the other data points with the 
#same colours that are used for the other plots
scatter = plt.scatter(centroidsDF["X"],centroidsDF["Y"],cmap="jet",
                      c=centroidPred)
#Creates a legend for the centroids plot
legend1=plt.legend(*scatter.legend_elements())
#Shows all of the plots on one axis
plt.show()


#DBSCAN
#Initialising the DBSCAN model and training it on the data frame
dbscan = DBSCAN(eps=1,min_samples=5).fit(dbscanPCA)
#Getting the labels that are produced from training the data
DBSCANLabels=dbscan.labels_
#Getting the labels after testing the model on the Data Frame
dbscanpred=dbscan.fit_predict(dbscanPCA)

#Setting the labels of the graph
plt.xlabel("PCA1")
plt.ylabel("PCA2")
plt.title("DBSCAN")
#Creating the scatter plot for the DBSCAN model
dbscanScatter=plt.scatter(dbscanPCA[:,0], dbscanPCA[:,1],cmap="jet", c=dbscanpred)
#Creates a legend using the information from the DBSCAN scatter plot
dbscanLegend = plt.legend(*dbscanScatter.legend_elements())
plt.show()


#WSS and BSS

#Storing the cluster labels in a Data Frame
KmeansPD = pd.DataFrame(pred)
#Creating a new Data Frame from the PCA data
pcaPD = pd.DataFrame(pca,columns=["PCA 1", "PCA 2"])
#Adding the cluster labels to the pcaPD Data Frame
pcaPD["Cluster"]=KmeansPD
#pcaPD.to_csv("./data/PCA_Data.csv")

#Seperating the Data by the cluster label
pcaCluster0 = pcaPD[pcaPD["Cluster"] == 0] 
pcaCluster1 = pcaPD[pcaPD["Cluster"] == 1] 
pcaCluster2 = pcaPD[pcaPD["Cluster"] == 2] 
pcaCluster3 = pcaPD[pcaPD["Cluster"] == 3] 
pcaCluster4 = pcaPD[pcaPD["Cluster"] == 4]

#Dropping the Cluster column
pcaCluster0 = pcaCluster0.drop(["Cluster"], axis=1)
pcaCluster1 = pcaCluster1.drop(["Cluster"], axis=1)
pcaCluster2 = pcaCluster2.drop(["Cluster"], axis=1)
pcaCluster3 = pcaCluster3.drop(["Cluster"], axis=1)
pcaCluster4 = pcaCluster4.drop(["Cluster"], axis=1)

#Resetting all of the indexes so they all start from 0 and go up by 1
pcaCluster0 = pcaCluster0.reset_index(drop=True)
pcaCluster1 = pcaCluster1.reset_index(drop=True) 
pcaCluster2 = pcaCluster2.reset_index(drop=True)
pcaCluster3 = pcaCluster3.reset_index(drop=True)
pcaCluster4 = pcaCluster4.reset_index(drop=True)


#Counting the number of rows in the Data Frame
count=pcaCluster0["PCA 1"].count()
#Making each column into a list so that it is easier to apply functions
pca1Cluster0 = pcaCluster0["PCA 1"].tolist()
pca2Cluster0 = pcaCluster0["PCA 2"].tolist()
#Adding a check value to identify which cluster the data belongs to
pca1Cluster0.append(0)


count1=pcaCluster1["PCA 1"].count()
pca1Cluster1 = pcaCluster1["PCA 1"].tolist()
pca2Cluster1 = pcaCluster1["PCA 2"].tolist()
pca1Cluster1.append(1)


count2=pcaCluster2["PCA 1"].count()
pca1Cluster2 = pcaCluster2["PCA 1"].tolist()
pca2Cluster2 = pcaCluster2["PCA 2"].tolist()
pca1Cluster2.append(2)

count3=pcaCluster3["PCA 1"].count()
pca1Cluster3 = pcaCluster3["PCA 1"].tolist()
pca2Cluster3 = pcaCluster3["PCA 2"].tolist()
pca1Cluster3.append(3)

count4=pcaCluster4["PCA 1"].count()
pca1Cluster4 = pcaCluster4["PCA 1"].tolist()
pca2Cluster4 = pcaCluster4["PCA 2"].tolist()
pca1Cluster4.append(4)

#Calling the distance calculation function for each cluster
EDCluster0 = EuclideanDistance(pca1Cluster0, pca2Cluster0, count, centroids)
EDCluster1 = EuclideanDistance(pca1Cluster1, pca2Cluster1, count1, centroids)
EDCluster2 = EuclideanDistance(pca1Cluster2, pca2Cluster2, count2, centroids)
EDCluster3 = EuclideanDistance(pca1Cluster3, pca2Cluster3, count3, centroids)
EDCluster4 = EuclideanDistance(pca1Cluster4, pca2Cluster4, count4, centroids)

#Saving the results in a Data Frame
EDCluster0DF = pd.DataFrame(EDCluster0, columns=["Cluster 0"])
EDCluster1DF = pd.DataFrame(EDCluster1,columns=["Cluster 1"])
EDCluster2DF = pd.DataFrame(EDCluster2,columns=["Cluster 2"])
EDCluster3DF = pd.DataFrame(EDCluster3,columns=["Cluster 3"])
EDCluster4DF = pd.DataFrame(EDCluster4,columns=["Cluster 4"])

#Storing the sum of each of the Data Frames as floats
sumC0 = float(EDCluster0DF.sum()) 
sumC1 = float(EDCluster1DF.sum())
sumC2 = float(EDCluster2DF.sum())
sumC3 = float(EDCluster3DF.sum())
sumC4 = float(EDCluster4DF.sum())

#The final step in calculating the WSS. Adding together all of the sum values
sseTotal = sumC0 + sumC1 + sumC2 + sumC3 + sumC4


#Finding the co-ordinates of the sample mean
pca1Mean = pcaPD["PCA 1"].mean()
pca2Mean = pcaPD["PCA 2"].mean()

#Implementing the BSS equation using the sample mean value calculated above
#and the co-ordinates of the centroids and then multiplying it by the counts
#for each cluster
centroid0=(((pca1Mean-centroids[0,0])**2)+(pca2Mean-centroids[0,1])**2)*count
centroid1=(((pca1Mean-centroids[1,0])**2)+(pca2Mean-centroids[1,1])**2)*count1
centroid2=(((pca1Mean-centroids[2,0])**2)+(pca2Mean-centroids[2,1])**2)*count2
centroid3=(((pca1Mean-centroids[3,0])**2)+(pca2Mean-centroids[3,1])**2)*count3
centroid4=(((pca1Mean-centroids[4,0])**2)+(pca2Mean-centroids[4,1])**2)*count4
#Adding together the BSS calculations to get the final BSS value
bss = centroid0+centroid1+centroid2+centroid3+centroid4

#Displaying the WSS and BSS values
print("When K is equal to 5:","\nWSS:",sseTotal, "\nBSS:", bss)



#Resetting the index of the data so it starts from 0
newtraitData=traitData.reset_index(drop=True)
#Adding the class labels as a new column
newtraitData["Cluster"]=KmeansPD
#Saving the data as csv files
newtraitData.to_csv("./data/Updated_Trait_Data.csv")
centroidsDF.to_csv("./data/Centroids.csv")
