# Final Year Project

This repository contains the code used in my final year project as well as all of the data files that are needed for the code to work. The main section of the code is in the FYP-final.py file. This file contains the clustering and cluster validity code. The Elbow-method.py file contains all of the code that was used to perform the elbow method on the data set. The Trait_analysis.py file contains the code that was used to perform the trait analysis on the data.

Inside the Website folder you will find the two files that are used for the web application the README file in the folder will explain which file should be run first.

[https://github.com/seang3004/FYP](https://github.com/seang3004/FYP) This URL is a link to my personal repository for this project that also contains the data files and is used in the websites to recover the data sets.
