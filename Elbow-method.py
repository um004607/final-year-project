# -*- coding: utf-8 -*-
"""
Created on Sun Jan 29 11:31:49 2023

@author: seang
"""

from sklearn.cluster import KMeans
import numpy as np
import csv
import matplotlib.pyplot as plt
from numpy import genfromtxt
from sklearn.decomposition import PCA
import pandas as pd

#Importing the csv file and saving it as a pandas data frame
traitDF = pd.read_csv('./data/Feature_result.csv',dtype=int,delimiter=',')
#Getting a random 1% of the data set
traitData = traitDF.sample(frac=0.01)



pca = PCA(n_components=2).fit_transform(traitData)

inertias = []

#Creates a for loop that runs 100 times as the likelihood that the elbow point
#will be larger than 100 is very small
for i in range(1,100):
#Performs clustering on each value from 1-100 and calculates the inertia that
#the point has
    kmeans = KMeans(n_clusters=i).fit(pca)
    kmeans.fit(pca)
#Assigns the inertia to an array
    inertias.append(kmeans.inertia_)
#Plots a graph showing the inertia on the y axis and the number of clusters on
#the x axis
plt.plot(range(1,100), inertias, marker='o')
plt.title('Elbow method 100')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()



inertias = []
for i in range(1,50):
#Performs clustering on each value from 1-50 and calculates the inertia that
#the point has
    kmeans = KMeans(n_clusters=i).fit(pca)
    kmeans.fit(pca)
#Assigns the inertia to an array
    inertias.append(kmeans.inertia_)
#Plots a graph showing the inertia on the y axis and the number of clusters on
#the x axis
plt.plot(range(1,50), inertias, marker='o')
plt.title('Elbow method 50')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()




inertias = []
for i in range(1,25):
#Performs clustering on each value from 1-25 and calculates the inertia that
#the point has
    kmeans = KMeans(n_clusters=i).fit(pca)
    kmeans.fit(pca)
#Assigns the inertia to an array
    inertias.append(kmeans.inertia_)
#Plots a graph showing the inertia on the y axis and the number of clusters on
#the x axis
plt.plot(range(1,25), inertias, marker='o')
plt.title('Elbow method 25')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()




inertias = []
for i in range(1,15):
#Performs clustering on each value from 1-15 and calculates the inertia that
#the point has
    kmeans = KMeans(n_clusters=i).fit(pca)
    kmeans.fit(pca)
#Assigns the inertia to an array
    inertias.append(kmeans.inertia_)
#Plots a graph showing the inertia on the y axis and the number of clusters on
#the x axis
plt.plot(range(1,15), inertias, marker='o')
plt.title('Elbow method 15')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()

inertias = []
for i in range(1,10):
#Performs clustering on each value from 1-100 and calculates the inertia that
#the point has
    kmeans = KMeans(n_clusters=i).fit(pca)
    kmeans.fit(pca)
#Assigns the inertia to an array
    inertias.append(kmeans.inertia_)
#Plots a graph showing the inertia on the y axis and the number of clusters on
#the x axis
plt.plot(range(1,10), inertias, marker='o')
plt.title('Elbow method 10')
plt.xlabel('Number of clusters')
plt.ylabel('Inertia')
plt.show()

