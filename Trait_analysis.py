# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 15:53:50 2023

@author: seang
"""

from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
import math

#Reading the data from the csv file
trait_data = pd.read_csv("./data/Updated_Trait_Data.csv")
#Dropping the unnecessary column
trait_data = trait_data.drop(columns=["Unnamed: 0"])

#Seperating the data into the clusters
cluster0 = trait_data[trait_data["Cluster"]==0]
cluster1 = trait_data[trait_data["Cluster"]==1]
cluster2 = trait_data[trait_data["Cluster"]==2]
cluster3 = trait_data[trait_data["Cluster"]==3]

#Finding the mean value of each column
meanCluster0=cluster0.mean()
meanCluster1=cluster1.mean()
meanCluster2=cluster2.mean()
meanCluster3=cluster3.mean()

#Displaying the mean values for each cluster
print("\nCluster 0\n",meanCluster0)
print("\nCluster 1\n",meanCluster1)
print("\nCluster 2\n",meanCluster2)
print("\nCluster 3\n",meanCluster3)

